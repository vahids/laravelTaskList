<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Task;

class TaskController extends Controller
{
    //

    public function index(Request $request){
    	$tasks = $request->user()->tasks()->get();

    	return view('tasks.index')->with(compact('tasks'));
    }

    public function store(Request $request){

    	$this->validate($request, [
    			'name' => 'required|max:255',
    		]);

    	$request->user()->tasks()->create([
    			'name' => $request->name,
    		]);
    	return redirect('/tasks');
    }


    public function destroy(Request $request, Task $task) {
        $this->authorize('destroy', $task);
        $task->delete();
        return redirect('/tasks');
    }

    public function __construct(){
    	$this->middleware('auth');
    }
}
